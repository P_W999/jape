/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.exceptions;

/**
 * A RuntimeException to indicate that the initialization of the test has failed.
 *
 * @author P_W999
 */
public class InitializationException extends RuntimeException {
    /**
     * {@inheritDoc}
     */
    public InitializationException(Exception e) {
        super(e);
    }
}
