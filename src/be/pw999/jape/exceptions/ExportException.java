/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.exceptions;

/**
 * A RuntimeException to indicate that the export of the test results has failed.
 *
 * @author P_W999
 */
public class ExportException extends RuntimeException {
    /**
     * {@inheritDoc}
     */
    public ExportException(Exception e) {
        super(e);
    }
}
