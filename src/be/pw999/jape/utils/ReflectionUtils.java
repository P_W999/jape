/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;


/**
 * A utility class for reflection.
 *
 * @author P_W999
 */
public class ReflectionUtils {

    /**
     * Hidden constructor for utility class.
     */
    private ReflectionUtils() {
    }

    /**
     * Returns the annotation of type annotation on method m.
     * <p>
     * This method will look through the complete class hierarchy until it finds the first declaration of the annotation.
     * </p>
     *
     * @param annotation the type of annotation that must be returned
     * @param m          the method from which the annotation should be returned (including declarations on super classes)
     * @param <T>        the type of annotation that will be returned
     * @return the annotation or null if not found.
     */
    public static <T extends Annotation> T getAnnotation(Class<T> annotation, Method m) {
        T a = m.getAnnotation(annotation);
        Class superclass = m.getDeclaringClass();
        Method method;
        while (a == null && superclass != null) {
            superclass = superclass.getSuperclass();
            if (superclass != null) {
                try {
                    method = superclass.getMethod(m.getName());
                } catch (NoSuchMethodException e) {
                    method = null;
                }
                if (method != null) {
                    a = method.getAnnotation(annotation);
                }
            }

        }
        return a;
    }
}
