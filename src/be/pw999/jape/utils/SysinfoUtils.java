/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.utils;

import be.pw999.jape.constants.Settings;
import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.*;

/**
 * Utility class to gather system info (using SIGAR)
 *
 * @author P_W999
 */
public class SysinfoUtils {
    /**
     * Hidden constructor for utility class.
     */
    private SysinfoUtils() {
    }


    public static Map<String, String> getAllInfo(Sigar sigar) throws SigarException {
        Map<String, String> map = new TreeMap<String, String>();
        CpuInfo cpu = sigar.getCpuInfoList()[0];
        map.put("CPU Vendor", cpu.getVendor());
        map.put("CPU Model", cpu.getModel());
        map.put("CPU Mhz", String.valueOf(cpu.getMhz()));
        map.put("Java Vendor", System.getProperty("java.vendor"));
        map.put("Java Version", System.getProperty("java.version"));
        map.put("Java Arch DM", System.getProperty("sun.arch.data.model"));
        map.put("OS Name", System.getProperty("os.name"));
        map.put("OS Version", System.getProperty("os.version"));
        map.put("OS Arch", System.getProperty("os.arch"));
        map.put("Input args", getInputArgs());
        map.put("Max Mem (runtime)", getMaxMem());
        map.putAll(getMemoryFromMbeanServer());
        map.put(" Number of loops", Integer.toString(Settings.TEST_LOOPS) + " (Integer.MAX_VALUE/" + (Integer.MAX_VALUE / Settings.TEST_LOOPS) + ")");
        map.put(" Initial object size", Integer.toString(Settings.LIST_SIZE));

        map.put(" Ms time precision", getMsTime());
        return map;
    }

    private static String getInputArgs() {
        Collection<String> args = ManagementFactory.getRuntimeMXBean().getInputArguments();
        StringBuilder sb = new StringBuilder();
        for (String s : args) {
            sb.append(s).append(" ");
        }
        return sb.toString();
    }

    private static String getMaxMem() {
        return Long.toString(Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "Mb";
    }

    private static Map<String, String> getMemoryFromMbeanServer() {
        List<MemoryPoolMXBean> memoryBeans = ManagementFactory.getPlatformMXBeans(MemoryPoolMXBean.class);
        Map<String, String> memoryMap = new HashMap<String, String>(memoryBeans.size());
        for (MemoryPoolMXBean memoryBean : memoryBeans) {
            memoryMap.put("Max " + memoryBean.getName() + " (mxb)", Long.toString(memoryBean.getUsage().getMax() / (1024 * 1024)) + "Mb");
        }
        return memoryMap;
    }

    public static String getMsTime() {
        long nanos = 0;
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() <= start) {
            ;
        }
        nanos = (System.currentTimeMillis() - start);

        return Long.toString(nanos) + "ms";
    }

}
