/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.utils;

import be.pw999.jape.constants.Settings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Handles Garbage Collection calling.
 * <p>
 * This class will ensure that the GC won't be called too often.
 * </p>
 */
public class GCManager {

    private static final Log log = LogFactory.getLog(GCManager.class);

    /**
     * The GC is only called every X request.
     */
    private static final int X = Math.max(Settings.TEST_LOOPS / 2500, 20);
    /**
     * c amount of request were made since the last explicit GC
     */
    private static int c = 0;

    /**
     * Request a garbage collection
     */
    public static void gc() {
        if (c++ > X) {
            log.info("Calling Garbage Collection after " + X + " requests");
            System.gc();
            c = 0;
        }
    }


    /**
     * Force a garbage collection.
     */
    public static void forceGC() {
        log.info("Forced Garbage Collection. Call count was " + c);
        c = 0;
        System.gc();
    }
}
