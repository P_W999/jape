package be.pw999.jape.classloader;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author P_W999
 */
public class JaPeClassLoader extends ClassLoader {

    private static Map<String, Class<?>> classes = new WeakHashMap<String, Class<?>>();

    public JaPeClassLoader(ClassLoader parent, URL[] urls) {
        super(parent);
        System.out.println("*** [JaPeClassLoader init ]" + parent.getClass().getName());
    }

    //http://k2java.blogspot.be/2011/04/writing-custom-class-loader-in-java.html

    public void clear() {
        classes.clear();
        System.out.println("*** [JaPeClassLoader cleared ]");
    }

    /**
     * Loads a given class from .class file just like
     * the default ClassLoader. This method could be
     * changed to load the class over network from some
     * other server or from the database.
     *
     * @param name Fully qualified class name
     */
    private Class<?> getClass(String name) throws ClassNotFoundException {
        //System.out.println("[ClassLoader] getClass " + name);
        // We are getting a name that looks like
        // com.vaani.ClassToLoad
        // and we have to convert it into the .class file name like
        // com/vaani/ClassToLoad.class
        String file = name.replace('.', File.separatorChar) + ".class";

        byte[] b = null;
        try {
            // This loads the byte code data from the file
            b = loadClassData(file);
            // defineClass is inherited from the ClassLoader class
            // and converts the byte array into a Class
            Class<?> c = defineClass(name, b, 0, b.length);
            resolveClass(c);
            classes.put(name, c);
            return c;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Every request for a class passes through this method.
     * If the requested class is in "com.vaani" package,
     * it will load it using the
     * CustomClassLoader.getClass() method.
     * If not, it will use the super.loadClass() method
     * which in turn will pass the request to the parent.
     *
     * @param name Full class name
     */
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {

        if (name.startsWith("be.pw999.jape")) {
            if (classes.containsKey(name)) {
                return classes.get(name);
            }
            System.out.println("[ClassLoader] load " + name);
            Class<?> c = getClass(name);
            classes.put(name, c);
            return c;

        }
        return super.loadClass(name);
    }


    /**
     * Loads a given file (presumably .class) into a byte array.
     * The file should be accessible as a resource, for example
     * it could be located on the classpath.
     *
     * @param name File name to load
     * @return Byte array read from the file
     * @throws IOException Is thrown when there
     *                     was some problem reading the file
     */
    private byte[] loadClassData(String name) throws IOException {
        // Opening the file
        InputStream stream = getClass().getClassLoader().getResourceAsStream(name);
        int size = stream.available();
        byte buff[] = new byte[size];
        DataInputStream in = new DataInputStream(stream);
        // Reading the binary data
        in.readFully(buff);
        in.close();
        return buff;
    }

}
