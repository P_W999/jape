/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.constants;

/**
 * A class to centralize test ids.
 *
 * @author P_W999
 */
public class TestIds {

    public static final int LIST_ARRAYLIST = 10000;
    public static final int LIST_LINKEDLIST = 20000;
    public static final int LIST_STACK = 30000;

    public static final int COLLECTION_HASHSET = 40000;
    public static final int COLLECTION_LINKEDHASHSET = 50000;
    public static final int COLLECTION_TREESET = 60000;

    public static final int ARRAY = 70000;

    public static final int MAP_HASHMAP = 80000;
    public static final int MAP_LINKEDHASHMAP = 90000;
    public static final int MAP_TREEMAP = 100000;
    public static final int MAP_WEAKHASHMAP = 110000;

    public static final int ARIT_INTEGER = 120000;
    public static final int ARIT_DOUBLE = 140000;


    public static final int STRING_STRING = 150000;
    public static final int STRING_STRINGBUFFER = 160000;
    public static final int STRING_STRINGBUILDER = 170000;
    public static final int STRING_STRINGBUFFER_CAPACITY = 180000;
    public static final int STRING_STRINGBUILDER_CAPACITY = 190000;

    public static final int BOOLEAN_BOOLEAN = 200000;
    public static final int LOOPS_LOOPS = 210000;
    public static final int BASE_BASE = 220000;

}
