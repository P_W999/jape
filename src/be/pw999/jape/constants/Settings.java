/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.constants;

/**
 * A class to centralize the benchmark settings.
 *
 * @author P_W999
 */
public class Settings {

    /**
     * In tests that involve collections, array and other iterable objects, this value determines the initial size of the collections.
     * <p>
     * Changing this value requires a re-evaluation of tests annotated with {@link be.pw999.jape.annotations.NotInfinite} .
     * For example, a test that will delete objects from a collection can only be run a limited amount of times until it reaches an {@link IndexOutOfBoundsException}.
     * </p>
     * <p>
     * The higher this value is, the more memory you'll need.
     * </p>
     */
    public static int LIST_SIZE = 1024;
    /**
     * Determines how often each test method should be ran.
     * <p>
     * The higher the value, the slower the test and the higher the accuracy of the results. A higher value may also require a larger heap to prevent frequent garbage collections.
     * </p>
     */
    public static final int TEST_LOOPS = 100000;

    /**
     * Indicates how often each test should be repeated.
     * <p>
     * Each run, the value of #LIST_SIZE will be quadrupled.
     * </p>
     */
    public static final int TEST_REPEAT = 2;

}
