/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.models;

import be.pw999.jape.tests.ITest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;

/**
 * An object to collect test results.
 * <p>
 * Comparison of Results is based on the #declaringClass and #declaredMethod value, duration is not used for comparison.
 * </p>
 *
 * @author P_W999
 */
public class Result {

    private static final Log log = LogFactory.getLog(Result.class);

    /**
     * The test-class
     */
    private final Class<? extends ITest> declaringClass;

    /**
     * The test-method
     */
    private final Method declaredMethod;

    /**
     * The amount of <b>milliseconds</b> it took to execute the test {@link be.pw999.jape.constants.Settings#TEST_LOOPS} times.
     */
    private final double duration;

    /**
     * Constructor to initialize the result object completely.
     *
     * @param declaringClass the class for the test
     * @param declaredMethod the test method
     * @param duration       test execution time. See {@link #duration}
     */
    public Result(Class<? extends ITest> declaringClass, Method declaredMethod, double duration) {
        this.declaringClass = declaringClass;
        this.declaredMethod = declaredMethod;
        this.duration = duration;
        if (duration < 0 && duration != Double.MIN_VALUE) {
            log.warn("Negative duration found" + this.toString());
        }
    }

    /**
     * Gets {@link #declaringClass}
     *
     * @return the declaring class.
     */
    public Class<? extends ITest> getDeclaringClass() {
        return declaringClass;
    }

    /**
     * Gets {@link #declaredMethod}
     *
     * @return the declared method.
     */
    public Method getDeclaredMethod() {
        return declaredMethod;
    }

    /**
     * Gets {@link #duration}
     *
     * @return the test execution duration.
     */
    public double getDuration() {
        return duration;
    }

    /**
     * Converts this object to a String.
     * <p>
     * To improve readability, this method will return the {@link #declaringClass} it's simpleName and the {@link #declaredMethod} it's name.
     * </p>
     *
     * @return the object formatted as String
     */
    @Override
    public String toString() {
        return "Result{" +
                "declaringClass=" + declaringClass.getSimpleName() +
                ", declaredMethod=" + declaredMethod.getName() +
                ", duration=" + duration +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (!declaredMethod.getName().equals(result.declaredMethod.getName())) return false;
        if (!declaringClass.getName().equals(result.declaringClass.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = declaringClass.getName().hashCode();
        result = 31 * result + declaredMethod.getName().hashCode();
        return result;
    }
}
