/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.models.comparator;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.models.Result;
import be.pw999.jape.utils.ReflectionUtils;

import java.util.Comparator;

/**
 * Comparator which can be used to sort Results based on the test-method id.
 *
 * @see {@link be.pw999.jape.annotations.Test#value()}
 */
public class ResultMethodIdComparator implements Comparator<Result> {
    @Override
    public int compare(Result o1, Result o2) {
        Double o1MethodId = ReflectionUtils.getAnnotation(Test.class, o1.getDeclaredMethod()).value();
        Double o2MethodId = ReflectionUtils.getAnnotation(Test.class, o2.getDeclaredMethod()).value();
        return o1MethodId.compareTo(o2MethodId);
    }
}
