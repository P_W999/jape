/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.models.comparator;

import be.pw999.jape.models.Result;
import be.pw999.jape.tests.utils.TestUtils;

import java.util.Comparator;

/**
 * Comparator which can be used to sort Results based on the full test id.
 *
 * @see {@link be.pw999.jape.annotations.Test#value()}
 * @see {@link be.pw999.jape.annotations.Test} for more info on test ids.
 */
public class ResultFullTestIdComparator implements Comparator<Result> {
    @Override
    public int compare(Result o1, Result o2) {
        Double o1TestId = TestUtils.getTestId(o1.getDeclaringClass(), o1.getDeclaredMethod());
        Double o2TestId = TestUtils.getTestId(o2.getDeclaringClass(), o2.getDeclaredMethod());
        return o1TestId.compareTo(o2TestId);
    }
}
