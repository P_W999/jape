/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape;

import be.pw999.jape.classloader.JaPeClassLoader;
import be.pw999.jape.constants.Settings;
import be.pw999.jape.utils.GCManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * The class responsible for executing the tests.
 *
 * @author P_W999
 */
public class Main {

    private static Log log = LogFactory.getLog(Main.class);


    static {
        System.getProperties().setProperty(LogFactory.class.getName(),
                Logger.class.getName());
    }

    public static void main(String[] args) {
        try {
            log.info("Warming up the engines ... 10 seconds sleep time");
            GCManager.forceGC();
            System.runFinalization();
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            log.error("Can't sleep", e);
        }

        if (args != null && args.length == 1) {
            Settings.LIST_SIZE = Integer.parseInt(args[0]);
        }

        for (int k = 0; k < Settings.TEST_REPEAT; ++k) {
            try {
                JaPeClassLoader classLoader = new JaPeClassLoader(log.getClass().getClassLoader(), new URL[]{new File("./").toURI().toURL()});
                {
                    Class<?> runnerClazz = classLoader.loadClass("be.pw999.jape.TestRunner");
                    Class<?> settingsClass = classLoader.loadClass("be.pw999.jape.constants.Settings");
                    settingsClass.getDeclaredField("LIST_SIZE").setInt(null, Settings.LIST_SIZE * Double.valueOf(Math.pow(4.0, k)).intValue());

                    Object runner = runnerClazz.newInstance();
                    Method m = runnerClazz.getMethod("run");
                    m.invoke(runner);
                    //runner.run(args);
                    m = null;
                    runner = null;
                }
                classLoader.clear();
            } catch (ClassNotFoundException e) {
                log.error("Dang :", e);
            } catch (InstantiationException e) {
                log.error("Dang :", e);
            } catch (IllegalAccessException e) {
                log.error("Dang :", e);
            } catch (InvocationTargetException e) {
                log.error("Dang :", e);
            } catch (NoSuchMethodException e) {
                log.error("Dang :", e);
            } catch (MalformedURLException e) {
                log.error("Dang :", e);
            } catch (NoSuchFieldException e) {
                log.error("Dang :", e);
            }
            GCManager.forceGC();
        }

    }


}
