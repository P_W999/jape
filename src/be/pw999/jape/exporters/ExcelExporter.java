/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.exporters;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.exceptions.ExportException;
import be.pw999.jape.models.Result;
import be.pw999.jape.models.comparator.ResultFullTestIdComparator;
import be.pw999.jape.tests.utils.TestUtils;
import be.pw999.jape.utils.ReflectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Class to export list of results to an Excel file.
 *
 * @author P_W999
 */
public class ExcelExporter {

    private final Log log = LogFactory.getLog(getClass());

    public File export(Map<String, List<Result>> results, Set<Map.Entry<String, String>> sysInfo, String fileName) {
        log.info("Starting export to excel");
        File f = null;
        FileOutputStream fileOut = null;
        try {
            f = new File("workbook.xls");
            Workbook wb;
            if (f.exists()) {
                wb = new HSSFWorkbook(new FileInputStream("workbook.xls"));
            } else {
                wb = new HSSFWorkbook();
            }

            Sheet sysSHeet = getOrCreateSheet("SysInfo", wb);
            log.info("Creating sheet " + sysSHeet.getSheetName());
            int count = 0;
            for (Map.Entry<String, String> entry : sysInfo) {
                Row row = sysSHeet.createRow(count++);
                row.createCell(0).setCellValue(entry.getKey());
                row.createCell(1).setCellValue(entry.getValue());
            }
            sysSHeet.autoSizeColumn(0);
            sysSHeet.autoSizeColumn(1);

            //Plain export
            for (Map.Entry<String, List<Result>> entry : results.entrySet()) {
                Sheet sheet = getOrCreateSheet(entry.getKey(), wb);
                log.info("Creating sheet " + sheet.getSheetName());

                Row header = sheet.createRow(0);
                header.createCell(0).setCellValue("Test ID");
                header.createCell(1).setCellValue("Test Class");
                header.createCell(2).setCellValue("Test Description");
                header.createCell(3).setCellValue("Test Time (ms)");


                int l = entry.getValue().size();
                Collections.sort(entry.getValue(), new ResultFullTestIdComparator());
                for (int i = 0; i < l; ++i) {
                    Result result = entry.getValue().get(i);
                    double testId = TestUtils.getTestId(result.getDeclaringClass(), result.getDeclaredMethod());
                    String description = ReflectionUtils.getAnnotation(Test.class, result.getDeclaredMethod()).description();
                    double time = result.getDuration();
                    Row resultRow = sheet.createRow(i + 1);
                    resultRow.createCell(0).setCellValue(testId);
                    resultRow.createCell(1).setCellValue(result.getDeclaringClass().getSimpleName());
                    resultRow.createCell(2).setCellValue(description);
                    resultRow.createCell(3).setCellValue(time);
                }
                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
                sheet.autoSizeColumn(3);
            }


            //Comparable export
            for (Map.Entry<String, List<Result>> entry : results.entrySet()) {
                Sheet sheet = getOrCreateSheet(entry.getKey() + "(group)", wb);
                log.info("Creating sheet " + sheet.getSheetName());

                int l = entry.getValue().size();
                Collections.sort(entry.getValue(), new ResultFullTestIdComparator());

                //Find all test descriptions and class names
                Set<String> testDescriptions = new TreeSet<String>();
                Set<String> classNames = new TreeSet<String>();
                Map<String, Double> minVal = new HashMap<String, Double>();
                for (int i = 0; i < l; ++i) {
                    String desc = ReflectionUtils.getAnnotation(Test.class, entry.getValue().get(i).getDeclaredMethod()).description();
                    testDescriptions.add(desc);
                    classNames.add(entry.getValue().get(i).getDeclaringClass().getSimpleName());

                    if (minVal.containsKey(desc) && (minVal.get(desc) > entry.getValue().get(i).getDuration())) {
                        minVal.put(desc, entry.getValue().get(i).getDuration());
                    } else if (!minVal.containsKey(desc)) {
                        minVal.put(desc, entry.getValue().get(i).getDuration());
                    }
                }

                //Generate a row/col mapping
                Map<String, Integer> rowMapping = new TreeMap<String, Integer>();
                Map<String, Integer> colMapping = new TreeMap<String, Integer>();

                int z = 1;
                for (String s : testDescriptions) {
                    rowMapping.put(s, z++);
                }
                z = 1;
                for (String s : classNames) {
                    colMapping.put(s, z++);
                }

                //Init cells and stuff
                for (Integer i : rowMapping.values()) {
                    Row row = sheet.createRow(i);
                    for (int j : colMapping.values()) {
                        row.createCell(j);
                    }
                }

                //Create headers
                Row header = sheet.createRow(0);
                for (String s : colMapping.keySet()) {
                    header.createCell(colMapping.get(s)).setCellValue(s);
                }

                for (String s : rowMapping.keySet()) {
                    Row row = sheet.getRow(rowMapping.get(s));
                    row.createCell(0).setCellValue(s);
                }

                //CellStyle style = wb.createCellStyle();
                //DataFormat format = wb.createDataFormat();
                //style.setDataFormat(format.getFormat("0.00%"));
                //Write results
                for (int i = 0; i < l; ++i) {
                    Result result = entry.getValue().get(i);
                    String desc = ReflectionUtils.getAnnotation(Test.class, entry.getValue().get(i).getDeclaredMethod()).description();
                    Row row = sheet.getRow(rowMapping.get(desc));
                    Cell cell = row.getCell(colMapping.get(result.getDeclaringClass().getSimpleName()));
                    cell.setCellValue(result.getDuration());
                    //cell.setCellStyle(style);
                }

                sheet.autoSizeColumn(0);
                for (Integer i : colMapping.values()) {
                    sheet.autoSizeColumn(i);
                }
            }
            fileOut = new FileOutputStream(fileName);
            wb.write(fileOut);
        } catch (IOException e) {
            throw new ExportException(e);
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException e) {
                    log.error("Failed to close filOut");
                    throw new ExportException(e);
                }
            }
        }
        log.info("Finished exporting to Excel");
        return f;
    }


    private Sheet getOrCreateSheet(String name, Workbook wb) {
        if (wb.getSheet(name) != null) {
            return wb.getSheet(name);
        } else {
            return wb.createSheet(name);
        }
    }

}
