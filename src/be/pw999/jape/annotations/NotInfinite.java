/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.annotations;

import java.lang.annotation.*;

/**
 * This annotation indicates that the test method can not run infinitely.
 * <p>
 * This annotation is usually used on methods that add or delete objects, requiring a re-initialization every x invocations.
 * Thus, every <i>invocations</i> times the test method is called, the {@link be.pw999.jape.tests.ITest#init()} method will be called to reset the test object.
 * </p>
 *
 * @author P_W999
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
@Documented
public @interface NotInfinite {

    /**
     * A value to indicate how many times the method can run without requiring re-initialization of the test object.
     * <p>
     * The value may not be 0.
     * </p>
     *
     * @return amount of invocation.
     */
    public int invocations();

    /**
     * Indicates that NO garbage collection should be ran.
     * <p>
     * Default value is no, so GC will be called.
     * </p>
     *
     * @return whatever you set
     */
    public boolean noGC() default false;

}


