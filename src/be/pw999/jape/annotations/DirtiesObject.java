/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.annotations;

import java.lang.annotation.*;

/**
 * This annotation indicates that the test method may dirty the test object, probably influencing test that may follow.
 * <p>
 * After the test method is called, the {@link be.pw999.jape.tests.ITest#init()} will be called to reinitialize the object.
 * </p>
 *
 * @author P_W999
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
@Documented
public @interface DirtiesObject {
}
