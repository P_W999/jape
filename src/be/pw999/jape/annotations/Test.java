/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.annotations;

import java.lang.annotation.*;

/**
 * Indicates a test method.
 * <p>
 * Each test method should have a unique value and description. It's up to the developer to ensure that these values are unique.
 * In order to allow inheritance and abstraction of test methods, the final test-id will be determined by the value in {@link #value()} and the value returned by {@link be.pw999.jape.tests.ITest#getTestId()}.
 * </p>
 * <p>
 * Consider the following class hierarchy:
 * <ul>
 * <li>public abstract SuperClass implements ITest</li>
 * <li>public ClassA extends SuperClass</li>
 * <li>public ClassB extends SuperClass</li>
 * </ul>
 * SuperClass does not implement the method {@link be.pw999.jape.tests.ITest#getTestId()} but implements a test method
 * <pre>
 *     {@code
 *      {@literal @}Test( value = 1.0, description = "Something generic to test")
 *      public void testGeneric() {
 *          ; //do something
 *      }
 *     }
 *     </pre>
 * The implementation of getTestId on ClassA returns 100 and on ClassB it returns 200.<br />
 * The resulting test id's are:
 * <ul>
 * <li>ClassA: 100 + 1.0 = 101</li>
 * <li>ClassB: 200 + 1.0 = 201</li>
 * </ul>
 * </p>
 *
 * @author P_W999
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
@Documented
public @interface Test {
    /**
     * A unique test-id inside a given test-class. See {@link Test} documentation for more information about the test-ids.
     *
     * @return a test id
     */
    public double value();

    /**
     * Brief description of the test.
     *
     * @return a test description
     */
    public String description();
}
