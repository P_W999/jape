/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.annotations;

import java.lang.annotation.*;

/**
 * Annotation which can be used to group test classes in a test suite.
 *
 * @author P_W999
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Documented
public @interface TestSuite {

    /**
     * A name for the test-suite.
     *
     * @return a name
     */
    public String name();
}
