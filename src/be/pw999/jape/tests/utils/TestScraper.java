/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.utils;

import be.pw999.jape.tests.ITest;
import be.pw999.jape.tests.impl.BooleanTest;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * Utility class to find all tests.
 * <p>
 * All tests are supposed to reside in the package be.pw999.jape.tests.impl.. and should implement {@link ITest}
 * Test method should be annotated with {@link be.pw999.jape.annotations.Test}.
 * </p>
 *
 * @author P_W999
 */
public class TestScraper {

    /**
     * Hidden constructor for utility class.
     */
    private TestScraper() {
    }

    /**
     * Looks for test classes in the package be.pw999.jape.tests.impl.. .
     * <p>
     * Every public, non-abstract implementation of {@link ITest} will be returned by this method.
     * </p>
     *
     * @return an array of test classes.
     */
    public static Class<? extends ITest>[] getClasses() {
        if (1 != 1) {
            return new Class[]{BooleanTest.class};
        }
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .filterInputsBy(new FilterBuilder().includePackage("be.pw999.jape.tests.impl"))
                .setUrls(ClasspathHelper.forPackage("be.pw999.jape.tests.impl"))
                .setScanners(new SubTypesScanner())
        );
        Set<Class<? extends ITest>> allClasses = reflections.getSubTypesOf(ITest.class);
        Set<Class<? extends ITest>> classes = new HashSet<Class<? extends ITest>>();
        for (Class<? extends ITest> clazz : allClasses) {
            if ((clazz.getModifiers() & Modifier.ABSTRACT) == 0) {
                classes.add(clazz);
            }
        }
        return classes.toArray(new Class[0]);
    }

}
