/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.utils;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.tests.ITest;
import be.pw999.jape.utils.ReflectionUtils;

import java.lang.reflect.Method;

/**
 * A utility class which can be used in tests.
 *
 * @author P_W999
 */
public final class TestUtils {

    /**
     * Hidden constructor for utility class.
     */
    private TestUtils() {
    }

    /**
     * Returns a test-string which can be used for preparation of String-tests, Collection-tests or any other tests that requires Strings.
     * <p>
     * Be aware that this method involves String concatenation and conversion of int to String. This method may not be used in tests itself.
     * </p>
     *
     * @param i an integer to alter the returned String.
     * @return a String with value "VALUE_IN_COLLECTION: {i}"
     */
    public static String getTestString(int i) {
        return "VALUE_IN_COLLECTION: " + Integer.toString(i);
    }

    public static double getTestId(Class<? extends ITest> clazz, Method method) {
        try {
            double classId = clazz.newInstance().getTestId();
            double methodId = ReflectionUtils.getAnnotation(Test.class, method).value();
            return classId + methodId;
        } catch (InstantiationException e) {
            return -1;
        } catch (IllegalAccessException e) {
            return -1;
        }
    }
}
