/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests;

import be.pw999.jape.annotations.DirtiesObject;
import be.pw999.jape.annotations.NotInfinite;
import be.pw999.jape.annotations.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 4:11 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractListTest<T extends List<String>> extends AbstractCollectionTest<T> {


    @Test(value = 6.0, description = "For loop by index")
    public void testForLoop() {
        for (int i = 0; i < read.size(); ++i) {
            z = read.get(i);
        }
    }

    @Test(value = 6.5, description = "For loop by index with cached length")
    public void testForLoopCachedLength() {
        int l = read.size();
        for (int i = 0; i < l; ++i) {
            z = read.get(i);
        }
    }

    @Test(value = 6.0, description = "Remove an object by index")
    @DirtiesObject
    @NotInfinite(invocations = 900)
    public void testRemoveByIndex() {
        write.remove(10);
    }

    @Test(value = 6.0, description = "Add an object by index")
    @DirtiesObject
    @NotInfinite(invocations = 2000)
    public void testAddByIndex() {
        write.add(10, "abcdefghijklmnopqrstuvwxyz");
    }


}
