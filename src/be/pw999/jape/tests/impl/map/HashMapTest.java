/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.impl.map;

import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractMapTest;
import be.pw999.jape.tests.ITest;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 7:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class HashMapTest extends AbstractMapTest<HashMap<String, Double>> implements ITest {


    @Override
    public double getTestId() {
        return TestIds.MAP_HASHMAP;
    }
}
