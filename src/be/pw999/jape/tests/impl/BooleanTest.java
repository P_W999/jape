/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.tests.impl;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractTest;
import be.pw999.jape.tests.ITest;

/**
 * Tests boolean and Boolean performance.
 *
 * @author P_W999
 */
public class BooleanTest extends AbstractTest implements ITest {

    private Boolean object = Boolean.FALSE;
    private boolean primitive = false;

    @Test(value = 1.0, description = "Invert primitive boolean")
    public void testInvertPrimitive() {
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive; //10
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive; //20
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive; //30
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive;
        primitive = !primitive; //40
    }

    @Test(value = 2.0, description = "Invert object Boolean")
    public void testInvertObject() {
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;   //10
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;   //20
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;   //30
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;
        object = !object;   //40
    }

    @Override
    public void init() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public double getTestId() {
        return TestIds.BOOLEAN_BOOLEAN;
    }
}
