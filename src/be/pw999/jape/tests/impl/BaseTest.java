/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.tests.impl;

import be.pw999.jape.annotations.NotInfinite;
import be.pw999.jape.annotations.Test;
import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractTest;
import be.pw999.jape.tests.ITest;

/**
 * Just a base test for overhead measurements
 *
 * @author P_W999
 */
public class BaseTest extends AbstractTest implements ITest {

    @Test(value = 1.0, description = "Method invocation time via reflection (overhead)")
    public void testOverheadMethodCall() {
        ;
    }

    @Test(value = 2.0, description = "Method invocation time via reflection, NotInfinite=900")
    @NotInfinite(invocations = 900)
    public void testOverheadNotInfinite900() {
        ;
    }

    @Test(value = 3.0, description = "Method invocation time via reflection, NotInfinite=1")
    @NotInfinite(invocations = 1)
    public void testOverheadNotInfinite1() {
        ;
    }

    @Override
    public void init() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public double getTestId() {
        return TestIds.BASE_BASE;
    }
}
