/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.impl.arithmetic;

import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractArithmeticTest;
import be.pw999.jape.tests.ITest;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class DoubleTest extends AbstractArithmeticTest implements ITest {
    private final Double FIVE = Double.valueOf(5.2d);
    private final Double TEN = Double.valueOf(10.1d);

    private Double o = null;
    private double p = 0;

    @Override
    public void testAdd() {
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //5
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //10
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //20
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //30
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //40
    }

    @Override
    public void testSubtract() {
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //5
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //10
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //20
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //30
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //40
    }

    @Override
    public void testMultiply() {
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //5
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //10
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //20
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //30
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //40
    }

    @Override
    public void testDivide() {
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //5
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //10
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //20
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //30
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //40
    }

    @Override
    public void testAddInstanceOf() {
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);   //20
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) + Double.valueOf(10.1d);   //40
    }

    @Override
    public void testSubtractInstanceOf() {
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);   //20
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) - Double.valueOf(10.1d);   //40
    }

    @Override
    public void testMultiplyInstanceOf() {
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);   //20
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) * Double.valueOf(10.1d);   //40
    }

    @Override
    public void testDivideInstanceOf() {
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);   //20
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);   //5
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);   //10
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);
        o = Double.valueOf(5.2d) / Double.valueOf(10.1d);   //40
    }

    @Override
    public void testAddPrimitives() {
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;   //5
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;   //10
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;   //20
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;   //300
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;
        p = 5.2d + 10.1d;   //40
    }

    @Override
    public void testSubtractPrimitives() {
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;   //5
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;   //10
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;   //20
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;   //300
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;
        p = 5.2d - 10.1d;   //40
    }

    @Override
    public void testMultiplyPrimitives() {
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;   //5
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;   //10
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;   //20
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;   //300
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;
        p = 5.2d * 10.1d;   //40
    }

    @Override
    public void testDividePrimitives() {
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;   //5
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;   //10
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;   //20
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;   //300
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;
        p = 5.2d / 10.1d;   //40
    }

    @Override
    public void init() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public double getTestId() {
        return TestIds.ARIT_DOUBLE;
    }
}
