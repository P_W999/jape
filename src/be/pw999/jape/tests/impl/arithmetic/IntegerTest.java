/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.impl.arithmetic;

import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractArithmeticTest;
import be.pw999.jape.tests.ITest;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 7:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntegerTest extends AbstractArithmeticTest implements ITest {
    private final Integer FIVE = Integer.valueOf(5);
    private final Integer TEN = Integer.valueOf(10);

    private Integer o = null;
    private int p = 0;

    @Override
    public void testAdd() {
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //5
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //10
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //20
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //30
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN;
        o = FIVE + TEN; //40
    }

    @Override
    public void testSubtract() {
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //5
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //10
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //20
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //30
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN;
        o = FIVE - TEN; //40
    }

    @Override
    public void testMultiply() {
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //5
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //10
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //20
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //30
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN;
        o = FIVE * TEN; //40
    }

    @Override
    public void testDivide() {
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //5
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //10
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //20
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //30
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN;
        o = FIVE / TEN; //40
    }

    @Override
    public void testAddInstanceOf() {
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);   //5
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);   //10
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);   //20
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);   //5
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);   //10
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);
        o = Integer.valueOf(5) + Integer.valueOf(10);   //40
    }

    @Override
    public void testSubtractInstanceOf() {
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);   //5
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);   //10
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);   //20
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);   //5
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);   //10
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);
        o = Integer.valueOf(5) - Integer.valueOf(10);   //40
    }

    @Override
    public void testMultiplyInstanceOf() {
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);   //5
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);   //10
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);   //20
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);   //5
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);   //10
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);
        o = Integer.valueOf(5) * Integer.valueOf(10);   //40
    }

    @Override
    public void testDivideInstanceOf() {
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);   //5
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);   //10
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);   //20
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);   //5
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);   //10
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);
        o = Integer.valueOf(5) / Integer.valueOf(10);   //40
    }

    @Override
    public void testAddPrimitives() {
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;   //5
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;   //10
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;   //20
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;   //300
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;
        p = 5 + 10;   //40
    }

    @Override
    public void testSubtractPrimitives() {
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;   //5
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;   //10
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;   //20
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;   //300
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;
        p = 5 - 10;   //40
    }

    @Override
    public void testMultiplyPrimitives() {
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;   //5
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;   //10
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;   //20
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;   //300
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;
        p = 5 * 10;   //40
    }

    @Override
    public void testDividePrimitives() {
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;   //5
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;   //10
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;   //20
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;   //300
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;
        p = 5 / 10;   //40
    }

    @Override
    public void init() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public double getTestId() {
        return TestIds.ARIT_INTEGER;
    }
}
