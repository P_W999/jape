/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.tests.impl.string;

import be.pw999.jape.constants.Settings;
import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractStringTest;
import be.pw999.jape.tests.ITest;

/**
 * Created with IntelliJ IDEA.
 * User: phillip
 * Date: 12/25/13
 * Time: 4:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringBuilderTest extends AbstractStringTest<StringBuilder> implements ITest {


    @Override
    public void testStringConcatenation() {
        write.append(" ");
    }

    @Override
    public void testToString() {
        read.toString();
    }

    @Override
    public void init() {
        write = new StringBuilder();
        read = new StringBuilder();
        for (int i = 0; i < Settings.LIST_SIZE; ++i) {
            read.append(Integer.toString(i));
        }
    }

    @Override
    public double getTestId() {
        return TestIds.STRING_STRINGBUILDER;
    }
}
