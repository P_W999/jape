/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.tests.impl;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.constants.Settings;
import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractTest;
import be.pw999.jape.tests.ITest;

/**
 * @author P_W999
 */
public class BasicLoopTest extends AbstractTest implements ITest {

    private int z = 0;
    int count = 0;

    @Override
    public void init() {

    }

    @Override
    public void destroy() {

    }

    @Test(value = 1.0, description = "For loop with i++ and <")
    public void testForLoopPrimitivePlusPlusLess() {
        for (int i = 0; i < Settings.LIST_SIZE; i++) {
            z = i;
        }
    }

    @Test(value = 2.0, description = "For loop with ++i and <")
    public void testForLoopPlusPlusPrimitiveLess() {
        for (int i = 0; i < Settings.LIST_SIZE; ++i) {
            z = i;
        }
    }

    @Test(value = 3.0, description = "For loop with i++ and <=")
    public void testForLoopPrimitivePlusPlusLessOrEqual() {
        for (int i = 1; i <= Settings.LIST_SIZE; i++) {
            z = i;
        }
    }

    @Test(value = 4.0, description = "For loop with ++i and <=")
    public void testForLoopPlusPlusPrimitiveLessOrEqual() {
        for (int i = 1; i <= Settings.LIST_SIZE; ++i) {
            z = i;
        }
    }

    @Override
    public double getTestId() {
        return TestIds.LOOPS_LOOPS;
    }
}
