/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.impl.collection;

import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractListTest;
import be.pw999.jape.tests.ITest;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 6:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class LinkedListTest extends AbstractListTest<LinkedList<String>> implements ITest {

    @Override
    public double getTestId() {
        return TestIds.LIST_LINKEDLIST;
    }
}
