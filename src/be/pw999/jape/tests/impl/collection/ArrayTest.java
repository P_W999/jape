/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests.impl.collection;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.annotations.TestSuite;
import be.pw999.jape.constants.Settings;
import be.pw999.jape.constants.TestIds;
import be.pw999.jape.tests.AbstractTest;
import be.pw999.jape.tests.ITest;
import be.pw999.jape.tests.utils.TestUtils;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 6:43 PM
 * To change this template use File | Settings | File Templates.
 */
@TestSuite(name = "CollectionSuite")
public class ArrayTest extends AbstractTest implements ITest {

    protected String[] array = new String[Settings.LIST_SIZE];
    protected String z = null;

    @Override
    public void init() {
        for (int i = 0; i < Settings.LIST_SIZE; ++i) {
            array[i] = TestUtils.getTestString(i);
        }
    }

    @Override
    public void destroy() {
        array = null;
    }

    @Test(value = 1.0, description = "For loop by index")
    public void testForLoop() {
        for (int i = 0; i < array.length; ++i) {
            z = array[i];
        }
    }

    @Test(value = 2.0, description = "For loop by index with cached length")
    public void testForLoopCachedLength() {
        int l = array.length;
        for (int i = 0; i < l; ++i) {
            z = array[i];
        }
    }

    @Test(value = 3.0, description = "For loop enhanced")
    public void testEnhancedForLoop() {
        for (String s : array) {
            z = s;
        }
    }

    @Override
    public double getTestId() {
        return TestIds.ARRAY;
    }
}
