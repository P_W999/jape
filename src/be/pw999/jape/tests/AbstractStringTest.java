/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.tests;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.annotations.TestSuite;
import be.pw999.jape.exceptions.InitializationException;

/**
 * Created with IntelliJ IDEA.
 * User: phillip
 * Date: 12/25/13
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */
@TestSuite(name = "StringSuite")
public abstract class AbstractStringTest<T extends CharSequence> extends AbstractTest {

    protected T write;
    protected T read;
    protected String z = null;

    public AbstractStringTest() {
        try {
            write = (T) getParametrisedClass().newInstance();
            read = (T) getParametrisedClass().newInstance();
        } catch (InstantiationException e) {
            throw new InitializationException(e);
        } catch (IllegalAccessException e) {
            throw new InitializationException(e);
        }
    }


    @Test(value = 1.0, description = "String concatenation")
    public abstract void testStringConcatenation();


    @Test(value = 2.0, description = "ToString()")
    public abstract void testToString();

    @Override
    public void destroy() {
        write = null;
        read = null;
        z = null;
    }

}
