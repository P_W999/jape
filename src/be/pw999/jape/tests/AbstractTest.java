/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests;


import be.pw999.jape.exceptions.InitializationException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Abstract test class offering some re-usable methods.
 *
 * @author P_W999
 */
public abstract class AbstractTest implements ITest {

    /**
     * Returns the first generic parameter of a test class.
     * <p>
     * Consider a test class:
     * <pre>
     *         {@code
     *
     *         public abstract MySuperTestClass&lt;T&gt; extends AbstractTest {
     *             // whatever suits your needs
     *         }
     *
     *         }
     *         then a call to getParametrisedClass will return the Class object for T.
     *     </pre>
     *
     * A an example implementation could be:
     * <pre>
     *         {@code
     *
     *         public void MyTestClass extends MySuperClass&lt;String&gt; {
     *             // more code and stuff here
     *         }
     *
     *         }
     *     </pre>
     *
     * for this implementation, getParametrisedClass will return String.class .
     * </p>
     *
     * @return the Class object representing T
     */
    Class getParametrisedClass() {
        try {
            ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
            if (parameterizedType.getActualTypeArguments()[0] instanceof Class) {
                return (Class) parameterizedType.getActualTypeArguments()[0];
            } else {
                Type tType = ((ParameterizedType) (parameterizedType.getActualTypeArguments()[0])).getRawType();
                return (Class) tType;
            }

        } catch (Exception e) {
            throw new InitializationException(e);
        }
    }

}
