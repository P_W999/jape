/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests;

import be.pw999.jape.annotations.DirtiesObject;
import be.pw999.jape.annotations.NotInfinite;
import be.pw999.jape.annotations.Test;
import be.pw999.jape.annotations.TestSuite;
import be.pw999.jape.constants.Settings;
import be.pw999.jape.exceptions.InitializationException;
import be.pw999.jape.tests.utils.TestUtils;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 3:56 PM
 * To change this template use File | Settings | File Templates.
 */
@TestSuite(name = "CollectionSuite")
public abstract class AbstractCollectionTest<T extends Collection<String>> extends AbstractTest {

    protected final T baseList;

    protected AbstractCollectionTest() {
        try {
            //http://www.artima.com/weblogs/viewpost.jsp?thread=208860
            baseList = (T) getParametrisedClass().newInstance();
            for (int i = 0; i < Settings.LIST_SIZE; ++i) {
                baseList.add(TestUtils.getTestString(i));
            }
        } catch (InstantiationException e) {
            throw new InitializationException(e);
        } catch (IllegalAccessException e) {
            throw new InitializationException(e);
        }
    }


    @Override
    public void init() {
        try {
            if (read == null) {
                read = (T) getParametrisedClass().newInstance();
            }
            if (write == null) {
                write = (T) getParametrisedClass().newInstance();
            }
            read.clear();
            write.clear();
            read.addAll(baseList);
            write.addAll(baseList);
        } catch (InstantiationException e) {
            throw new InitializationException(e);
        } catch (IllegalAccessException e) {
            throw new InitializationException(e);
        }
    }

    @Override
    public void destroy() {
        read.clear();
        write.clear();
        baseList.clear();
        read = null;
        write = null;
    }

    protected T read = null;
    protected T write = null;

    protected String z = null;

    protected final String objectToRemove = "___1___";

    @Test(value = 1.0, description = "For loop enhanced")
    public void testEnhancedForLoop() {
        for (String s : read) {
            z = s;
        }
    }

    @Test(value = 2.0, description = "Iterate with iterator")
    public void testIterator() {
        Iterator<String> it = read.iterator();
        while (it.hasNext()) {
            z = it.next();
        }
    }

    @Test(value = 3.0, description = "Convert to array and (For loop by index with cached length)")
    public void testToArrayForLoop() {
        String[] array = read.toArray(new String[0]);
        int l = array.length;
        for (int i = 0; i < l; ++i) {
            z = array[i];
        }
    }

    @Test(value = 4.0, description = "Clear collection")
    @DirtiesObject
    @NotInfinite(invocations = 1, noGC = true)
    public void testClear() {
        write.clear();
    }

    @Test(value = 5.0, description = "Add an object at last position")
    @DirtiesObject
    @NotInfinite(invocations = 2000)
    public void testAdd() {
        write.add("abcdefghijklmnopqrstuvwxyz");
    }

    @Test(value = 6.0, description = "Remove an object by object reference")
    @DirtiesObject
    @NotInfinite(invocations = 1, noGC = true)
    public void testRemoveByObject() {
        write.remove(objectToRemove);
    }


}
