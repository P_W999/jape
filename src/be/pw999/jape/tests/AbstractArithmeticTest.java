/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests;

import be.pw999.jape.annotations.Test;
import be.pw999.jape.annotations.TestSuite;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 7:27 PM
 * To change this template use File | Settings | File Templates.
 */
@TestSuite(name = "ArithmeticSuite")
public abstract class AbstractArithmeticTest<T extends Number> extends AbstractTest {

    @Test(value = 1.0, description = "Test addition with fixed objects")
    public abstract void testAdd();

    @Test(value = 2.0, description = "Test subtraction with fixed objects")
    public abstract void testSubtract();

    @Test(value = 3.0, description = "Test multiplication with fixed objects")
    public abstract void testMultiply();

    @Test(value = 4.0, description = "Test division with fixed objects")
    public abstract void testDivide();


    @Test(value = 5.0, description = "Test addition with instanceof objects")
    public abstract void testAddInstanceOf();

    @Test(value = 6.0, description = "Test subtraction with instanceof objects")
    public abstract void testSubtractInstanceOf();

    @Test(value = 7.0, description = "Test multiplication instanceof fixed objects")
    public abstract void testMultiplyInstanceOf();

    @Test(value = 8.0, description = "Test division with instanceof objects")
    public abstract void testDivideInstanceOf();


    @Test(value = 10.0, description = "Test addition with primitives")
    public abstract void testAddPrimitives();

    @Test(value = 11.0, description = "Test subtraction with primitives")
    public abstract void testSubtractPrimitives();

    @Test(value = 12.0, description = "Test multiplication with primitives")
    public abstract void testMultiplyPrimitives();

    @Test(value = 13.0, description = "Test division with primitives")
    public abstract void testDividePrimitives();
}
