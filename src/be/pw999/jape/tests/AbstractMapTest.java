/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests;

import be.pw999.jape.annotations.DirtiesObject;
import be.pw999.jape.annotations.NotInfinite;
import be.pw999.jape.annotations.Test;
import be.pw999.jape.annotations.TestSuite;
import be.pw999.jape.constants.Settings;
import be.pw999.jape.exceptions.InitializationException;
import be.pw999.jape.tests.utils.TestUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: P_W999
 * Date: 12/24/13
 * Time: 6:56 PM
 * To change this template use File | Settings | File Templates.
 */
@TestSuite(name = "MapSuite")
public abstract class AbstractMapTest<T extends Map<String, Double>> extends AbstractTest {

    protected final T baseMap;
    protected T read;
    protected T write;

    protected Double z = null;

    private final Double ONE = new Double(1d);
    private final String DELETE_KEY = TestUtils.getTestString(10);

    public AbstractMapTest() {
        try {
            //http://www.artima.com/weblogs/viewpost.jsp?thread=208860
            baseMap = (T) getParametrisedClass().newInstance();
            for (int i = 0; i < Settings.LIST_SIZE; ++i) {
                baseMap.put(TestUtils.getTestString(i), new Double(i));
            }
        } catch (InstantiationException e) {
            throw new InitializationException(e);
        } catch (IllegalAccessException e) {
            throw new InitializationException(e);
        }
    }

    @Override
    public void init() {
        try {
            if (read == null) {
                read = (T) getParametrisedClass().newInstance();
            }
            if (write == null) {
                write = (T) getParametrisedClass().newInstance();
            }
            read.clear();
            write.clear();
            read.putAll(baseMap);
            write.putAll(baseMap);
        } catch (InstantiationException e) {
            throw new InitializationException(e);
        } catch (IllegalAccessException e) {
            throw new InitializationException(e);
        }
    }

    @Override
    public void destroy() {
        read.clear();
        write.clear();
        baseMap.clear();
        read = null;
        write = null;
    }

    @Test(value = 1.0, description = "Loop map via entrySet using enhanced for-loop")
    public void testLoopEntrySetEnhancedForLoop() {
        Set<Map.Entry<String, Double>> set = read.entrySet();
        for (Map.Entry<String, Double> s : set) {
            z = s.getValue();
        }
    }

    @Test(value = 2.0, description = "Loop map via entrySet using iterator")
    public void testLoopEntrySetIterator() {
        Set<Map.Entry<String, Double>> set = read.entrySet();
        Iterator<Map.Entry<String, Double>> it = set.iterator();
        while (it.hasNext()) {
            z = it.next().getValue();
        }
    }

    @Test(value = 3.0, description = "Loop map via key look-up using enhanced for-loop")
    public void testLoopKeyLookUpEnhancedForLoop() {
        for (String s : read.keySet()) {
            z = read.get(s);
        }
    }

    @Test(value = 4.0, description = "Loop map via key look-up using iterator")
    public void testLoopKeyLookUpIterator() {
        Iterator<String> it = read.keySet().iterator();
        while (it.hasNext()) {
            z = read.get(it.next());
        }
    }

    @Test(value = 5.0, description = "Put a value")
    @DirtiesObject
    @NotInfinite(invocations = 2, noGC = true)   // 1 no-hit, 1 hit
    public void testPut() {
        write.put("1", ONE);
    }

    @Test(value = 6.0, description = "Clear a list")
    @DirtiesObject
    @NotInfinite(invocations = 1, noGC = true)
    public void testClear() {
        write.clear();
    }

    @Test(value = 7.0, description = "Clear a list")
    @DirtiesObject
    @NotInfinite(invocations = 1, noGC = true)
    public void testDelete() {
        write.remove(DELETE_KEY);
    }

}
