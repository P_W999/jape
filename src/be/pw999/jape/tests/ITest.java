/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.tests;


/**
 * This interface defines the default structure of a test.
 * <p>
 * A test class must explicitly  declare that it implements this interface, it is not enough that the interface is declared on an extend class.
 * A test class that does <b>not</b> declare that it implements this interface will <b>not</b> be found by the Reflections scanner.
 * </p>
 *
 * @author P_W999
 */
public interface ITest {

    /**
     * Method used to initialize the test object.
     * <p>
     * Be aware that this method may be called multiple times during the test object's lifecycle and it should always reset the test object to the same state.
     * </p>
     */
    public void init();

    /**
     * Method used to destroy the test object.
     * <p>
     * This method is only called when all test methods have finished. This method should (preferably) release all resources.
     * </p>
     */
    public void destroy();

    /**
     * Returns the unique id of the test class.
     *
     * @return the test id.
     * @see be.pw999.jape.annotations.Test for more information about how test id's are handled.
     */
    public double getTestId();

}
