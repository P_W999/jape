/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */

package be.pw999.jape.tests.impl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This test is only to ensure that every loop is executed an equal amount of times.
 * <p>
 * Don't run it: it'll fail because you'll have to add count++ to each test (count++ is <b>not</b> something that's part of the performance test)
 * </p>
 *
 * @author P_W999
 */
public class BasicLoopTestTest {

    private BasicLoopTest test;

    @Test
    public void testLoops() throws Exception {
        test = new BasicLoopTest();

        int testForLoopPrimitivePlusPlusLess = 0;
        int testForLoopPlusPlusPrimitiveLess = 0;
        int testForLoopPrimitivePlusPlusLessOrEqual = 0;
        int testForLoopPlusPlusPrimitiveLessOrEqual = 0;

        test.count = 0;
        test.testForLoopPrimitivePlusPlusLess();
        testForLoopPrimitivePlusPlusLess = test.count;

        test.count = 0;
        test.testForLoopPlusPlusPrimitiveLess();
        testForLoopPlusPlusPrimitiveLess = test.count;

        test.count = 0;
        test.testForLoopPrimitivePlusPlusLessOrEqual();
        testForLoopPrimitivePlusPlusLessOrEqual = test.count;

        test.count = 0;
        test.testForLoopPlusPlusPrimitiveLessOrEqual();
        testForLoopPlusPlusPrimitiveLessOrEqual = test.count;

        //expected - actual
        assertEquals(testForLoopPrimitivePlusPlusLess, testForLoopPlusPlusPrimitiveLess);
        assertEquals(testForLoopPrimitivePlusPlusLess, testForLoopPrimitivePlusPlusLessOrEqual);
        assertEquals(testForLoopPrimitivePlusPlusLess, testForLoopPlusPlusPrimitiveLessOrEqual);
    }


}
