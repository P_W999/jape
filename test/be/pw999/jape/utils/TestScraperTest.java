package be.pw999.jape.utils;

import be.pw999.jape.tests.ITest;
import be.pw999.jape.tests.utils.TestScraper;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test the TestScraper.
 * <p>
 * The tests are not 100% fool proof because adding performance tests should not break these unit tests.
 * </p>
 *
 * @author P_W999
 */
public class TestScraperTest {

    private static final int NUMBER_OF_TEST_I_ALREADY_MADE = 13;

    @Test
    public void testGetClasses() throws Exception {
        Class<? extends ITest>[] array = TestScraper.getClasses();
        assertNotNull("TestScraper return null", array);
        assertNotEquals("TestScraper did not find any class", 0l, array.length);
        assertTrue("TestScraper did not find enough classes", array.length >= NUMBER_OF_TEST_I_ALREADY_MADE);
    }
}
