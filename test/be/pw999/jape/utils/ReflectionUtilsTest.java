/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Belgium License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/be/deed.en_US.
 */
package be.pw999.jape.utils;

import be.pw999.jape.tests.AbstractArithmeticTest;
import be.pw999.jape.tests.impl.arithmetic.DoubleTest;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link ReflectionUtils} class.
 *
 * @author P_W999
 */
public class ReflectionUtilsTest {

    @Test
    public void testGetAnnotationClassMethod() throws NoSuchMethodException {
        Class clazz = DoubleTest.class;
        Method m = clazz.getMethod("testAdd");
        assertNotNull("Test preparation failed", m);
        be.pw999.jape.annotations.Test t = ReflectionUtils.getAnnotation(be.pw999.jape.annotations.Test.class, m);
        assertNotNull("ReflectionUtils failed to find annotation in super class", t);
    }

    @Test
    public void testGetAnnotationClassMethodTwo() throws NoSuchMethodException {
        Class clazz = AbstractArithmeticTest.class;
        Method m = clazz.getMethod("testAdd");
        assertNotNull("Test preparation failed", m);
        be.pw999.jape.annotations.Test t = ReflectionUtils.getAnnotation(be.pw999.jape.annotations.Test.class, m);
        assertNotNull("ReflectionUtils failed to find annotation on class itself", t);
    }

}
